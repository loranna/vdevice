package main

import (
	"gitlab.com/loranna/vdevice/cmd"
)

func main() {
	err := cmd.GenerateDocumentation()
	if err != nil {
		panic(err)
	}
}
