module gitlab.com/loranna/vdevice

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	github.com/gorilla/mux v1.7.4
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.6.3
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	gitlab.com/loranna/configuration v0.0.0-20201008184923-a9174680e41f
	gitlab.com/loranna/loranna v0.1.1
	gitlab.com/loranna/lorawan v0.16.214
	gitlab.com/loranna/venvironment v0.12.163
)
