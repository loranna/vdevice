package device

import (
	"fmt"

	"gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/lorawan/version"
	"gitlab.com/loranna/vdevice/device/types"
	v1_0_0_device "gitlab.com/loranna/vdevice/v1_0_0/device"
)

type Device interface {
	Uplink(confirmed bool, fport *uint8, frmpayload []byte) error
	JoinABP() error
	JoinOTAA() error
	Status() types.DeviceStatus
	SetDatarate(dr byte)
	SetDnCtr(ctr uint32)
	SetAdrackreq()
	SetLinkCheckReq()
	Close() error
	SetAdr(enable bool)
}

func CreateABP(s types.SendReceiver, deveui [8]byte, devaddr [4]byte, nwkskey, appskey [16]byte,
	r region.Region, vlorawan version.LoRaWANVersion, vregion version.RegionVersion) (Device, error) {
	switch vlorawan {
	case version.LoRaWANV1_0_0:
		switch vregion {
		case version.RegionV1_0_0:
			dev := v1_0_0_device.NewABP(s, deveui, devaddr, nwkskey, appskey, r)
			return dev, nil
		}
		return nil, fmt.Errorf("unknown region %s", vregion)
	}
	return nil, fmt.Errorf("unknown lorawan version %s", vlorawan)
}

func CreateOTAA(s types.SendReceiver, deveui [8]byte, appeui [8]byte, appkey [16]byte, r region.Region, vlorawan version.LoRaWANVersion, vregion version.RegionVersion) (Device, error) {
	switch vlorawan {
	case version.LoRaWANV1_0_0:
		switch vregion {
		case version.RegionV1_0_0:
			dev := v1_0_0_device.NewOTAA(s, deveui, appeui, appkey, r)
			return dev, nil
		}
		return nil, fmt.Errorf("unknown region %s", vregion)
	}
	return nil, fmt.Errorf("unknown lorawan version %s", vlorawan)
}

