package types

import (
	"fmt"
	"time"

	"gitlab.com/loranna/lorawan/region"
	"gitlab.com/loranna/vdevice/radio"
	"gitlab.com/loranna/venvironment/uplink"
)

type SendReceiver interface {
	Send(radio radio.Parameters, payload []byte, deveui string) error
	Receive() (<-chan uplink.WithMetadataToDevice, error)
}

type DeviceStatus struct {
	Deveui      string
	Appeui      string
	Devaddr     string
	Nwkskey     string
	Appskey     string
	Appkey      string
	Upcounter   uint32
	Downcounter uint32
	LastUp      time.Time
	LastDown    time.Time
	Datarate    byte
	Channels    []Channel
	Region      region.Region
	Joined      bool
}

type Channel struct {
	region.Channel
	Enabled bool
}

var IsBusy = fmt.Errorf("device is busy")
var NotJoined = fmt.Errorf("device is not joined")
