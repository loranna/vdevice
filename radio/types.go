package radio

import (
	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
)

type Parameters struct {
	radio.Modulation
	radio.CodeRate
	Bandwidth        uint32
	SpreadingFactor  uint8
	TxSignalStrength int
	Frequency        uint32
}

type Channel struct {
	Enabled bool
	region.Channel
}
