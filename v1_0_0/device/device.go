package device

import (
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	loranna_radio "gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/lorawan/region"
	regionv1_0_0 "gitlab.com/loranna/lorawan/region/v1_0_0"
	"gitlab.com/loranna/lorawan/region/v1_0_0/EU863_870"
	"gitlab.com/loranna/lorawan/region/v1_0_0/US902_928"
	"gitlab.com/loranna/lorawan/region/v1_0_1/AU915_928"
	"gitlab.com/loranna/lorawan/v1_0_0"
	"gitlab.com/loranna/lorawan/v1_0_0/create"
	"gitlab.com/loranna/lorawan/v1_0_0/parse"
	"gitlab.com/loranna/vdevice/device/types"
	"gitlab.com/loranna/vdevice/radio"
	"gitlab.com/loranna/venvironment/uplink"
)

type Device struct {
	sync.Mutex
	Deveui, Appeui           [8]byte
	Devaddr                  [4]byte
	Adr, Adrackreq, Ack      bool
	FcntUp, FcntDown         uint32
	Nwkskey, Appskey, Appkey [16]byte
	sender                   types.SendReceiver
	Joined, joining          bool
	rx1open, rx2open         bool
	busy, shouldretx         bool
	shouldlinkcheck          bool
	retx, alreadyretx        uint
	devnonce                 [2]byte
	cancelrx2                chan bool

	datarate                 byte
	txpower                  int
	linkadrans               v1_0_0.LinkADRAns
	lastUplink, lastDownlink time.Time
	channels                 []types.Channel
	region                   region.Region
	rx2dr                    uint8
}

func (dev *Device) Uplink(confirmed bool, fport *uint8, frmpayload []byte) error {
	dev.Lock()
	defer dev.Unlock()
	return dev.uplink(confirmed, fport, frmpayload, true)
}

func (dev *Device) uplink(confirmed bool, fport *uint8, frmpayload []byte, noretx bool) error {
	if !noretx {
		logrus.Debug("retxing")
	}
	if noretx && dev.busy {
		return types.IsBusy
	}
	dev.busy = true
	if !dev.Joined {
		return types.NotJoined
	}
	r, err := dev.calculateradioparams()
	if err != nil {
		return err
	}
	fopts := make([]v1_0_0.FrameOption, 0)
	if dev.linkadrans != nil {
		fopts = append(fopts, dev.linkadrans)
		dev.linkadrans = nil
	}
	if dev.shouldlinkcheck {
		req, err := create.LinkCheckReq()
		if err != nil {
			return err
		}
		fopts = append(fopts, req)
		dev.shouldlinkcheck = false
	}
	opts, err := v1_0_0.Fopts(fopts)
	if err != nil {
		return err
	}
	p, err := create.Uplink(confirmed, dev.Devaddr, dev.Adr, dev.Adrackreq, dev.Ack, uint16(dev.FcntUp), opts, fport, frmpayload, nil)
	if err != nil {
		return err
	}
	pe, err := p.Encrypt(dev.Nwkskey, dev.Appskey, byte(dev.FcntUp>>24), byte(dev.FcntUp>>16))
	if err != nil {
		return err
	}
	err = dev.sender.Send(r, pe.Bytes(), hex.EncodeToString(dev.Deveui[:]))
	if err != nil {
		return err
	}
	dev.lastUplink = time.Now()
	dev.Adrackreq = false
	dev.Ack = false
	dev.shouldretx = confirmed
	go dev.delayedopenofrx1(false)
	go dev.delayedopenofrx2(false)
	go dev.retryuplink(confirmed, fport, frmpayload)
	return nil
}

const tolarance = 50 * time.Millisecond

func (dev *Device) delayedopenofrx1(join bool) {
	delay := 1000*time.Millisecond - tolarance
	if join {
		delay += 4000 * time.Millisecond
	}
	<-time.After(delay)
	dev.Lock()
	defer dev.Unlock()
	dev.rx1open = true
	go dev.delayedcloseofrx1(join)
}

func (dev *Device) delayedcloseofrx1(join bool) {
	delay := 2 * tolarance
	<-time.After(delay)
	dev.Lock()
	defer dev.Unlock()
	dev.rx1open = false
	logrus.WithField("rxwindow", 1).Debug("Receive window is gone")
}

func (dev *Device) delayedopenofrx2(join bool) {
	delay := 2000*time.Millisecond - tolarance
	if join {
		delay += 4000 * time.Millisecond
	}
	select {
	case <-time.After(delay):
	case <-dev.cancelrx2:
		return
	}
	dev.Lock()
	defer dev.Unlock()
	dev.rx2open = true
	go dev.delayedcloseofrx2(join)
}

func (dev *Device) delayedcloseofrx2(join bool) {
	delay := 2 * tolarance
	select {
	case <-time.After(delay):
	case <-dev.cancelrx2:
		return
	}
	dev.Lock()
	defer dev.Unlock()
	dev.rx2open = false
	logrus.WithField("rxwindow", 2).Debug("Receive window is gone")
}

func (dev *Device) retryuplink(confirmed bool, fport *uint8, frmpayload []byte) {
	// rx2+ 2,7ms AKA longest downlink (51 bytes, SF12)
	<-time.After(4700 * time.Millisecond)
	dev.Lock()
	defer dev.Unlock()
	if dev.shouldretx {
		err := dev.uplink(confirmed, fport, frmpayload, false)
		if err != nil {
			logrus.Debugf("error on resend: %v", err)
		}
		if dev.retx <= dev.alreadyretx {
			//just give up and move on
			logrus.Debug("no more retx")
			dev.shouldretx = false
			dev.alreadyretx = 0
			dev.FcntUp++
			dev.busy = false
		} else {
			dev.alreadyretx++
		}
	} else {
		dev.FcntUp++
		dev.busy = false
	}
}

func (dev *Device) calculateradioparams() (radio.Parameters, error) {
	dr, err := regionv1_0_0.Datarate2sfbw(dev.region, dev.datarate)
	if err != nil {
		return radio.Parameters{}, err
	}
	freq, err := dev.nextFreq()
	if err != nil {
		return radio.Parameters{}, err
	}
	return radio.Parameters{
		Modulation:       dr.Modulation,
		CodeRate:         loranna_radio.CodeRate4_5,
		Bandwidth:        dr.Bandwidth,
		SpreadingFactor:  dr.SpreadingFactor,
		TxSignalStrength: dev.txpower,
		Frequency:        freq,
	}, nil
}

func (dev *Device) nextFreq() (uint32, error) {
	enabledchannels := make([]int, 0)
	for i, ch := range dev.channels {
		if ch.Enabled {
			enabledchannels = append(enabledchannels, i)
		}
	}
	if len(enabledchannels) == 0 {
		return 0, errors.New("no available channel")
	}
	index := rand.Int31n(int32(len(enabledchannels)))
	return dev.channels[enabledchannels[index]].Frequency, nil
}

func (dev *Device) downlink(downlinks <-chan uplink.WithMetadataToDevice) {
	go dev.monitordownlinks(downlinks)
}

func (dev *Device) monitordownlinks(downlinks <-chan uplink.WithMetadataToDevice) {
	for downlink := range downlinks {
		dev.Lock()
		if dev.rx1open {
			err := dev.handledownlink(downlink)
			if err != nil {
				logrus.Warningf("handling downlink failed: %v", err)
			}
			dev.cancelrx2 <- true
			dev.busy = false
		}
		if dev.rx2open {
			err := dev.handledownlink(downlink)
			if err != nil {
				logrus.Warningf("handling downlink failed: %v", err)
			}
			dev.busy = false
		}
		dev.Unlock()
	}
}

func (dev *Device) handledownlink(downlink uplink.WithMetadataToDevice) error {
	if dev.joining {
		dlstr, err := hex.DecodeString(downlink.GetPayload())
		if err != nil {
			return err
		}
		joinaccept, err := parse.JoinAcceptEncrypted(dlstr, dev.Appkey, dev.region)
		if err != nil {
			return err
		}
		logrus.WithField("decryptedpayload", strings.ToUpper(hex.EncodeToString(joinaccept.Bytes()))).Debug("received join accept")
		gotdevaddr := joinaccept.DevAddr()
		copy(dev.Devaddr[:], gotdevaddr[:])
		nwkskey, appskey, err := create.Keys(dev.Appkey, joinaccept.AppNonce(),
			joinaccept.NetID(), dev.devnonce)
		if err != nil {
			return err
		}
		dev.rx2dr = joinaccept.RX2DataRate()
		logrus.WithField("rx2dr", dev.rx2dr).Debug("rx2dr applied")
		if joinaccept.CFList() != nil {
			cflist := *joinaccept.CFList()
			switch dev.region {
			case region.EU863_870:
				ch4, ch5, ch6, ch7, ch8, err := EU863_870.ParseCFList(cflist)
				if err != nil {
					return err
				}
				dev.channels = append(dev.channels, types.Channel{region.Channel{Frequency: ch4, MinimumDataRate: 0, MaximumDataRate: 5}, true})
				dev.channels = append(dev.channels, types.Channel{region.Channel{Frequency: ch5, MinimumDataRate: 0, MaximumDataRate: 5}, true})
				dev.channels = append(dev.channels, types.Channel{region.Channel{Frequency: ch6, MinimumDataRate: 0, MaximumDataRate: 5}, true})
				dev.channels = append(dev.channels, types.Channel{region.Channel{Frequency: ch7, MinimumDataRate: 0, MaximumDataRate: 5}, true})
				dev.channels = append(dev.channels, types.Channel{region.Channel{Frequency: ch8, MinimumDataRate: 0, MaximumDataRate: 5}, true})
				logrus.WithFields(logrus.Fields{
					"f1": ch4,
					"f2": ch5,
					"f3": ch6,
					"f4": ch7,
					"f5": ch8,
				}).Debug("new frequencies were added")
			case region.US902_928:
				channels, err := US902_928.ParseCFList(*joinaccept.CFList())
				if err != nil {
					return err
				}
				dev.channels = channelToChannelUS(channels)
				message := strings.Builder{}
				message.WriteString("enabled channels: ")
				for i, channel := range dev.channels {
					if channel.Enabled {
						message.WriteString(fmt.Sprintf("%d,", i))
					}
				}
				m := message.String()
				logrus.Debugln(m[:len(m)-1])
			case region.AU915_928:
				channels, err := AU915_928.ParseCFList(*joinaccept.CFList())
				if err != nil {
					return err
				}
				dev.channels = channelToChannelUS(channels)
				message := strings.Builder{}
				message.WriteString("enabled channels: ")
				for i, channel := range dev.channels {
					if channel.Enabled {
						message.WriteString(fmt.Sprintf("%d,", i))
					}
				}
				m := message.String()
				logrus.Debugln(m[:len(m)-1])
			}
		}
		dev.Nwkskey = nwkskey
		dev.Appskey = appskey
		dev.joining = false
		dev.Joined = true
		logrus.Debug("device joined")
	} else {
		dlenc, err := dev.validateowner(downlink)
		if err != nil {
			return err
		}
		logrus.WithField("payload", downlink.GetPayload()).Debug("received downlink")
		dev.lastDownlink = time.Now()
		if dlenc.Ack() {
			dev.shouldretx = false
			logrus.Info("confirmation received")
		}
		if dlenc.Mtype() == v1_0_0.TypeConfirmedDataDown {
			dev.Ack = true
		}
		if port := dlenc.Fport(); port != nil {
			if *port == 0 {
				downlink, err := dlenc.Decrypt(dev.Nwkskey, dev.Appskey, byte(dev.FcntDown>>24), byte(dev.FcntDown>>16))
				if err != nil {
					return err
				}
				err = dev.processFrameOptions(downlink.Frmpayload())
				if err != nil {
					return err
				}
			} else {
				err = dev.processFrameOptions(dlenc.Fopts())
				if err != nil {
					return err
				}
				downlink, err := dlenc.Decrypt(dev.Nwkskey, dev.Appskey, byte(dev.FcntDown>>24), byte(dev.FcntDown>>16))
				if err != nil {
					return err
				}
				if downlink.Frmpayload() != nil {
					logrus.WithFields(logrus.Fields{
						"fport":      *downlink.Fport(),
						"frmpayload": downlink.Frmpayload(),
					}).Info("message received")
				}
			}
		} else {
			err = dev.processFrameOptions(dlenc.Fopts())
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (dev *Device) validateowner(downlink uplink.WithMetadataToDevice) (v1_0_0.DownlinkEncrypted, error) {
	dlstr, err := hex.DecodeString(downlink.GetPayload())
	if err != nil {
		return nil, err
	}
	dlenc, err := parse.DownlinkEncrypted(dlstr)
	if err != nil {
		return nil, err
	}
	valid, err := dlenc.MICValid(dev.Nwkskey, byte(dev.FcntDown>>24), byte(dev.FcntDown>>16))
	if err != nil {
		return nil, err
	}
	if !valid {
		return nil, fmt.Errorf("invalid mic")
	}
	if da := dlenc.Devaddr(); bytes.Compare(da[:], dev.Devaddr[:]) != 0 {
		return nil, fmt.Errorf("devaddrs not matching. Received: %X, Current: %X", da, dev.Devaddr)
	}
	valid = dev.checkandsetdownlinkcounter(dlenc.Fcnt())
	if !valid {
		return nil, fmt.Errorf("downlink counter invalid. Received: %d, Current: %d", dlenc.Fcnt(), dev.FcntDown)
	}
	return dlenc, nil
}

func (dev *Device) processFrameOptions(options []byte) error {
	opts, err := parse.FoptsDown(options)
	if err != nil {
		return err
	}
	var entry *logrus.Entry
	for _, opt := range opts {
		switch o := opt.(type) {
		case v1_0_0.LinkADRReq:
			err := dev.processLinkADRReq(o)
			if err != nil {
				return err
			}
			if entry != nil {
				entry = entry.WithField("linkadrreq", strings.ToUpper(hex.EncodeToString(o.Payload())))
			} else {
				entry = logrus.WithField("linkadrreq", strings.ToUpper(hex.EncodeToString(o.Payload())))
			}
		case v1_0_0.LinkCheckAns:
			if entry != nil {
				entry = entry.WithField("linkcheckans", strings.ToUpper(hex.EncodeToString(o.Payload())))
			} else {
				entry = logrus.WithField("linkcheckans", strings.ToUpper(hex.EncodeToString(o.Payload())))
			}
		default:
			return nil
		}
	}
	entry.Debug("processed frame options")
	return nil
}

func (dev *Device) processLinkADRReq(opt v1_0_0.LinkADRReq) error {
	var err error
	dr := opt.Datarate()
	if !dev.acceptabledr(dr) {
		linkadrans, err := create.LinkADRAns(true, false, true)
		if err != nil {
			return err
		}
		dev.linkadrans = linkadrans
		return fmt.Errorf("unacceptable datarate %d", dr)
	}
	txpower := opt.TxPower()
	if !dev.acceptabletxpower(txpower) {
		linkadrans, err := create.LinkADRAns(false, true, true)
		if err != nil {
			return err
		}
		dev.linkadrans = linkadrans
		return fmt.Errorf("unacceptable txpower %d", txpower)
	}
	if chmask, chmaskcntl := opt.Chmask(), opt.ChmaskCntl(); !dev.acceptablechmask(chmask, chmaskcntl) {
		linkadrans, err := create.LinkADRAns(true, true, false)
		if err != nil {
			return err
		}
		dev.linkadrans = linkadrans
		return fmt.Errorf("unacceptable chmask %s, chmaskcntl: %d", hex.EncodeToString(chmask[:]), chmaskcntl)
	}
	dev.datarate = dr
	dev.txpower, err = regionv1_0_0.TxpowerIndexToTxpower(dev.region, txpower)
	if err != nil {
		return err
	}
	linkadrans, err := create.LinkADRAns(true, true, true)
	if err != nil {
		return err
	}
	dev.linkadrans = linkadrans
	return nil
}

func (dev *Device) acceptabledr(dr byte) bool {
	for _, ch := range dev.channels {
		if ch.Enabled && ch.MinimumDataRate <= dr && dr <= ch.MaximumDataRate {
			return true
		}
	}
	return false
}

func (dev *Device) acceptabletxpower(txpower byte) bool {
	_, err := regionv1_0_0.TxpowerIndexToTxpower(dev.region, txpower)
	if err != nil {
		logrus.Debugf("not acceptable txpower index: %d", txpower)
		return false
	}
	return true
}

func (dev *Device) acceptablechmask(chmask [2]byte, chmaskcntl byte) bool {
	switch dev.region {
	case region.EU863_870:
		if chmaskcntl != 0 && chmaskcntl != 6 {
			return false
		}
		return true
	case region.US902_928:
		if chmaskcntl > 7 {
			return false
		}
		if chmaskcntl < 5 {
			startidx := chmaskcntl * 16
			chm := make([]bool, 16)
			fmt.Println(chmask)
			for i := 0; i < 16; i++ {
				idx := 0
				if i > 7 {
					idx = 1
				}
				if (chmask[idx]>>(i%8))&0x01 == 0x01 {
					chm[i] = true
				}
			}
			fmt.Println(chm)
			for i := startidx; i < startidx+16; i++ {
				if chm[i-startidx] {
					if dev.channels[i].Frequency > 0 {
						dev.channels[i].Enabled = chm[i-startidx]
					} else {
						fmt.Println("exits here")
						// callee shows proper error message, no need to do it here
						return false
					}
				} else {
					dev.channels[i].Enabled = chm[i-startidx]
				}
			}
		} else if chmaskcntl == 6 {
			//All 125 kHz ON ChMask applies to channels 64 to 71
			for i := 0; i < 64; i++ {
				if !dev.channels[i].Enabled {
					return false
				}
			}
			for i := 0; i < 8; i++ {
				if (chmask[0]>>i)&0x01 != 0x01 {
					if dev.channels[64+i].Frequency == 0 {
						return false
					}
				}
			}
		} else {
			//All 125 kHz OFF ChMask applies to channels 64 to 71
			for i := 0; i < 64; i++ {
				if dev.channels[i].Enabled {
					return false
				}
			}
			for i := 0; i < 8; i++ {
				if (chmask[0]>>i)&0x01 != 0x01 {
					if dev.channels[64+i].Frequency == 0 {
						return false
					}
				}
			}
		}
		return true
	case region.AU915_928:
		if chmaskcntl > 7 {
			return false
		}
		if chmaskcntl < 5 {
			startidx := chmaskcntl * 16
			chm := make([]bool, 16)
			fmt.Println(chmask)
			for i := 0; i < 16; i++ {
				idx := 0
				if i > 7 {
					idx = 1
				}
				if (chmask[idx]>>(i%8))&0x01 == 0x01 {
					chm[i] = true
				}
			}
			fmt.Println(chm)
			for i := startidx; i < startidx+16; i++ {
				if chm[i-startidx] {
					if dev.channels[i].Frequency > 0 {
						dev.channels[i].Enabled = chm[i-startidx]
					} else {
						fmt.Println("exits here")
						// callee shows proper error message, no need to do it here
						return false
					}
				} else {
					dev.channels[i].Enabled = chm[i-startidx]
				}
			}
		} else if chmaskcntl == 6 {
			//All 125 kHz ON ChMask applies to channels 64 to 71
			for i := 0; i < 64; i++ {
				if !dev.channels[i].Enabled {
					return false
				}
			}
			for i := 0; i < 8; i++ {
				if (chmask[0]>>i)&0x01 != 0x01 {
					if dev.channels[64+i].Frequency == 0 {
						return false
					}
				}
			}
		} else {
			//All 125 kHz OFF ChMask applies to channels 64 to 71
			for i := 0; i < 64; i++ {
				if dev.channels[i].Enabled {
					return false
				}
			}
			for i := 0; i < 8; i++ {
				if (chmask[0]>>i)&0x01 != 0x01 {
					if dev.channels[64+i].Frequency == 0 {
						return false
					}
				}
			}
		}
		return true
	default:
		logrus.Warnf("unknown region %s", dev.region)
		return false
	}
}

func (dev *Device) checkandsetdownlinkcounter(fcntdown uint16) bool {
	cnt := uint32(0)
	if dev.FcntDown == 0 {
		cnt = uint32(fcntdown)
	} else {
		diff16 := int(fcntdown) - int(dev.FcntDown&0x0000FFFF)
		if diff16 > 0 {
			cnt = dev.FcntDown + uint32(diff16)
		} else if diff16 < 0 {
			cnt = dev.FcntDown&0xFFFF0000 + 0x10000 + uint32(fcntdown)
		} else {
			return false
		}
		if cnt-dev.FcntDown >= 16384 {
			return false
		}
	}
	dev.FcntDown = cnt
	return true
}

func NewABP(s types.SendReceiver, deveui [8]byte, devaddr [4]byte, nwkskey, appskey [16]byte, r region.Region) *Device {
	dev := Device{}
	dev.sender = s
	dev.Mutex = sync.Mutex{}
	dev.Deveui = deveui
	dev.Devaddr = devaddr
	dev.Nwkskey = nwkskey
	dev.Appskey = appskey
	dev.retx = 3
	dev.alreadyretx = 0
	dev.cancelrx2 = make(chan bool, 1)
	dev.region = r
	dev.channels = defaultChannels(r)
	dev.txpower = regionv1_0_0.DefaultTxpower(r)
	ch, _ := s.Receive()
	dev.downlink(ch)
	return &dev
}

func defaultChannels(r region.Region) []types.Channel {
	channels := regionv1_0_0.DefaultChannels(r)
	chs := make([]types.Channel, len(channels))
	for i, channel := range channels {
		chs[i].Enabled = true
		chs[i].Frequency = channel.Frequency
		chs[i].MinimumDataRate = channel.MinimumDataRate
		chs[i].MaximumDataRate = channel.MaximumDataRate
	}
	return chs
}

func NewOTAA(s types.SendReceiver, deveui [8]byte, appeui [8]byte, appkey [16]byte, r region.Region) *Device {
	dev := Device{}
	dev.sender = s
	dev.Mutex = sync.Mutex{}
	dev.Deveui = deveui
	dev.Appeui = appeui
	dev.Appkey = appkey
	dev.retx = 3
	dev.alreadyretx = 0
	dev.cancelrx2 = make(chan bool, 1)
	dev.region = r
	dev.channels = defaultChannels(r)
	dev.txpower = regionv1_0_0.DefaultTxpower(r)
	ch, _ := s.Receive()
	dev.downlink(ch)
	return &dev
}

func channelToChannelUS(channels []region.Channel) []types.Channel {
	chs := make([]types.Channel, len(channels))
	for i, ch := range channels {
		enabled := false
		if ch.Frequency > 0 {
			enabled = true
		}
		chs[i] = types.Channel{region.Channel{Frequency: ch.Frequency, MinimumDataRate: 0, MaximumDataRate: 5}, enabled}
	}
	return chs
}

func (dev *Device) JoinABP() error {
	dev.Lock()
	defer dev.Unlock()
	dev.Joined = true
	logrus.Debug("device joined")
	return nil
}

func (dev *Device) JoinOTAA() error {
	dev.Lock()
	defer dev.Unlock()
	dev.busy = true
	dev.devnonce = [2]byte{byte(rand.Int()), byte(rand.Int())}
	joinrequest, err := create.JoinRequest(dev.Appeui, dev.Deveui, dev.devnonce, dev.Appkey)
	if err != nil {
		return err
	}
	r, err := dev.calculateradioparams()
	if err != nil {
		return err
	}
	jrbytes := joinrequest.Bytes()
	err = dev.sender.Send(r, jrbytes[:], hex.EncodeToString(dev.Deveui[:]))
	if err != nil {
		return err
	}
	dev.joining = true
	go dev.delayedopenofrx1(true)
	go dev.delayedopenofrx2(true)
	return nil
}

func (dev *Device) Close() error {
	close(dev.cancelrx2)
	return nil
}


func (dev *Device) Status() types.DeviceStatus {
	status := types.DeviceStatus{}
	status.Deveui = hex.EncodeToString(dev.Deveui[:])
	status.Appeui = hex.EncodeToString(dev.Appeui[:])
	status.Devaddr = hex.EncodeToString(dev.Devaddr[:])
	status.Nwkskey = hex.EncodeToString(dev.Nwkskey[:])
	status.Appskey = hex.EncodeToString(dev.Appskey[:])
	status.Appkey = hex.EncodeToString(dev.Appkey[:])
	status.Upcounter = dev.FcntUp
	status.Downcounter = dev.FcntDown
	status.LastUp = dev.lastUplink
	status.LastDown = dev.lastDownlink
	status.Region = dev.region
	status.Datarate = dev.datarate
	for _, channel := range dev.channels {
		status.Channels = append(status.Channels, types.Channel{
			channel.Channel, channel.Enabled,
		})
	}
	status.Joined = dev.Joined
	return status
}

func (dev *Device) SetDatarate(dr byte) {
	dev.Lock()
	defer dev.Unlock()
	dev.datarate = dr
	logrus.WithField("datarate", dr).Debugln("datarate set to new value")
}

func (dev *Device) SetDnCtr(ctr uint32) {
	dev.Lock()
	defer dev.Unlock()
	dev.FcntDown = ctr
	logrus.WithField("fcntdown", ctr).Debugln("fcntdown set to new value")
}

func (dev *Device) SetAdrackreq() {
	dev.Lock()
	defer dev.Unlock()
	dev.Adrackreq = true
	logrus.WithField("Adrackreq", true).Debugln("Adrackreq set to new value")
}

func (dev *Device) SetLinkCheckReq() {
	dev.Lock()
	defer dev.Unlock()
	dev.shouldlinkcheck = true
	logrus.WithField("shouldlinkcheck", true).Debugln("shouldlinkcheck set to new value")
}

func (dev *Device) SetAdr(enable bool) {
	dev.Lock()
	defer dev.Unlock()
	dev.Adr = enable
	logrus.WithField("setadr", true).Debugln("setadr set to new value")
}
