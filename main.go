package main

import (
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/vdevice/cmd"
)

func main() {
	delay := os.Getenv("START_DELAY")
	if len(delay) > 0 {
		delaysec, err := strconv.Atoi(delay)
		if err != nil {
			logrus.Warn(err)
			delaysec = 0
		}
		time.Sleep(time.Duration(delaysec) * time.Second)
	}
	logrus.SetLevel(logrus.TraceLevel)
	rand.Seed(time.Now().UnixNano())
	cmd.Execute()
}
