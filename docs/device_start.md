## device start

starts simulated device

### Synopsis

starts a simulated device which implements the LoRaWAN stack

```
device start [flags]
```

### Options

```
      --adr                           Enable/Disable the ADR (Adaptive Data Rate) in the device (default true)
      --appeui string                 Appeui of the device according to LoRaWAN specification (default "7AAAA52ADA4E33C8")
      --appkey string                 Appkey of the device according to LoRaWAN specification (default "656AE210976B6805D2CE6E34F52AAA47")
      --appskey string                Appskey of the device according to LoRaWAN specification (default "5588B99C8BCD6314114FAB441E297441")
      --counterdown int               Value of the downlink counter (FCntDown) in the device side
      --counterup int                 Value of the uplink counter (FCntUp) in the device side
      --devaddr string                Devaddr of the device according to LoRaWAN specification (default "93ED77C2")
      --deveui string                 Deveui of the device according to LoRaWAN specification (default "7AAAA59504A897E6")
      --deviceserverport int          Port of the device used to access it during execution (default 36000)
      --deviceupqueuename string      Name of the queue used for the devie uplink to the environment messages (default "uplinktoenvironment")
      --environmentserverport int     Port of the environment used to manipulate messages in both directions (default 35999)
      --environmentserverurl string   URL of the environment used to manipulate messages in both directions
      --gatewaydownqueuename string   Name of the queue used for the gateway downlink to the environment messages (default "downlinktoenvironment")
  -h, --help                          help for start
      --joinmethod string             Join method of the device according to LoRaWAN specification. Possible values: OTAA,ABP (default "OTAA")
      --latitude float                Latitude of the device (default 19.078076)
      --longitude float               Longitude of the device (default 47.515086)
      --loriotserverapikey string     API key of the LORIOT server used for data processing
      --loriotserverurl string        URL of the LORIOT server used for data processing
      --nwkskey string                Nwkskey of the device according to LoRaWAN specification (default "0BAB9E148A8D9D22772C8D7483572C94")
      --program int                   ID of the program used in the device
      --rabbitmqpassword string       Password of the RabbitMQ server used for data exchange (default "guest")
      --rabbitmqport int              Port of the RabbitMQ server used for data exchange (default 5672)
      --rabbitmqserverurl string      URL of the RabbitMQ server used for data exchange (default "localhost")
      --rabbitmqusername string       User Name of the RabbitMQ server used for data exchange (default "guest")
      --registermethod string         Registeration method of the device is LORIOT. Possible values: none,self,other,auto (default "auto")
      --rejoininterval int            How often must the device perform a join again after an unsuccessful join (default 10)
      --title string                  title or name of the device (default "loranna")
      --uplinkconfirmed               Enable/Disable confirmed uplink in the device
      --uplinkinterval int            How often should the device send uplinks. Unit is second (default 5)
      --uplinkpayload string          Value of the User payload (FRMPayload) in every uplink messages (default "0000")
      --uplinkport int                Value of port (FPort) used in every uplink message (default 22)
```

### SEE ALSO

* [device](#device)	 - simulated device

