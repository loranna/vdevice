## device version

Show version number

### Synopsis

Show version number

```
device version [flags]
```

### Options

```
  -h, --help   help for version
```

### SEE ALSO

* [device](#device)	 - simulated device

