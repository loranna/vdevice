## device

simulated device

### Synopsis

simulated device

### Options

```
  -h, --help   help for device
```

### SEE ALSO

* [device start](#device-start)	 - starts simulated device
* [device version](#device-version)	 - Show version number

