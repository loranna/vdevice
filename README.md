# Deviced

Deviced is a CLI that acts as a LoRaWAN device and able to send and receive messages according LoRaWAN specification.

## Additional information on Flags

### Program

#### Program 0

1. Parameter setting  
    1a. ABP version: Device sets deveui, devaddr, nwkskey, appskey, adr and initialized itself.  
    1b. OTAA version: Device sets deveui, appeui, appkey, adr and initialized itself.

2. It tries to join 5 times at most with wait time (flag `rejoininterval`) between joins

3. Uplinks issued until stopped and downlinks handled according to LoRaWAN specification

### LoRaWAN parameters

The following parameters generated randomly on start

- appeui
- appkey
- appskey
- devaddr
- nwkskey

Deveui has fix 3 starting bytes (`7AAAA5`), the others are randomly generated

Both LoRaWAN specification compliant join methods are supported

### Register method

`registermethod` can have the following states:

- none: device does not try to register itself, it assumes that LORIOT is ready to receive data from the device with the already setted parameters
- self: devics tries to register itself
- other: device expects that someone else registers deveui in LORIOT
- auto: device will try to register but no complain when deveui is already registered. (Note: no validation of keys happen)

# Commands

- [device](#device)
  - [start](#device-start)
  - [version](#device-version)
