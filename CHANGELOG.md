# CHANGELOG

- Lots of minor and major modifications

## 0.8.102

- Add documentation
- Add more LORIOT API support
- Change some command line flag's default parameter

## 0.7.95

- Add channels and CFList support
- Add `uplinkinterval` command line flag

## 0.6.87

- Add self-registration to LORIOT
- Migrate virtual device implementation into this project

## 0.6.72

- Add more logs
- Add conversion between radio parameters

## 0.6.69

- Add documentation
- Add one more program

## 0.5.61

- Add OTAA join program

## 0.4.55

- LORIOT self-registration was removed

## 0.3.49

- Add downlink support
- Add command line flag to select program

## 0.2.35

- Add basic support to self-register to LORIOT
- Add bunch of command line flags

## 0.1.4

- send uplinks to RabbitMQ
- provide endpoint for metadata
