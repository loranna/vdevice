package radioconv

import (
	"gitlab.com/loranna/lorawan/radio"
	"gitlab.com/loranna/venvironment/uplink"
)

func RadioModtouplink(m radio.Modulation) uplink.Modulation {
	switch m {
	case radio.ModulationFSK:
		return uplink.Modulation_MOD_FSK
	case radio.ModulationLoRa:
		return uplink.Modulation_MOD_LORA
	default:
		return uplink.Modulation_MOD_UNDEFINED
	}
}

func Inttobandwidth(b uint32) uplink.Bandwidth {
	switch b {
	case 125_000:
		return uplink.Bandwidth_BW_125KHZ
	case 15_600:
		return uplink.Bandwidth_BW_15K6HZ
	case 250_000:
		return uplink.Bandwidth_BW_250KHZ
	case 31_200:
		return uplink.Bandwidth_BW_31K2HZ
	case 500_000:
		return uplink.Bandwidth_BW_500KHZ
	case 62_500:
		return uplink.Bandwidth_BW_62K5HZ
	case 7_800:
		return uplink.Bandwidth_BW_7K8HZ
	default:
		return uplink.Bandwidth_BW_UNDEFINED
	}
}

func InttoSF(sf uint8) uplink.SpreadingFactor {
	switch sf {
	case 10:
		return uplink.SpreadingFactor_SF10
	case 11:
		return uplink.SpreadingFactor_SF11
	case 12:
		return uplink.SpreadingFactor_SF12
	case 7:
		return uplink.SpreadingFactor_SF7
	case 8:
		return uplink.SpreadingFactor_SF8
	case 9:
		return uplink.SpreadingFactor_SF9
	default:
		return uplink.SpreadingFactor_DR_UNDEFINED
	}
}

func RadioCRtoUplink(cr radio.CodeRate) uplink.CodeRate {
	switch cr {
	case radio.CodeRate4_5:
		return uplink.CodeRate_CR_LORA_4_5
	case radio.CodeRate4_6:
		return uplink.CodeRate_CR_LORA_4_6
	case radio.CodeRate4_7:
		return uplink.CodeRate_CR_LORA_4_7
	case radio.CodeRate4_8:
		return uplink.CodeRate_CR_LORA_4_8
	default:
		return uplink.CodeRate_CR_UNDEFINED
	}
}
