package cmd

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	conf "gitlab.com/loranna/configuration/device"
	"gitlab.com/loranna/loranna/loriot"
	"gitlab.com/loranna/lorawan/region"
	lversion "gitlab.com/loranna/lorawan/version"
	"gitlab.com/loranna/vdevice/broker"
	"gitlab.com/loranna/vdevice/device"
	"gitlab.com/loranna/vdevice/device/types"
	"gitlab.com/loranna/vdevice/util"
	"gitlab.com/loranna/venvironment/uplink"
)

var v = viper.New()

func init() {
	v.AllowEmptyEnv(true)
	conf.AddAndBindFlags(startCmd, v)
	conf.BindEnv(v)
	rootCmd.AddCommand(startCmd)
}

func environmentadd(config *conf.Config) error {
	requestBody, err := proto.Marshal(&me)
	if err != nil {
		return fmt.Errorf("add marshalling myself failed: %w", err)
	}
	url := fmt.Sprintf("http://%s:%d/device/add", config.ServerURL, config.Serverport)
	_, err = http.Post(url, "application/proto", bytes.NewBuffer(requestBody))
	if err != nil {
		return fmt.Errorf("add posting myself failed: %w", err)
	}
	return nil
}

func environmentremove(config *conf.Config) error {
	requestBody, err := proto.Marshal(&me)
	if err != nil {
		return fmt.Errorf("remove marshalling myself failed: %w", err)
	}
	url := fmt.Sprintf("http://%s:%d/device/remove", config.ServerURL, config.Serverport)
	_, err = http.Post(url, "application/proto", bytes.NewBuffer(requestBody))
	if err != nil {
		return fmt.Errorf("remove posting myself failed: %w", err)
	}
	return nil
}

func starthttpserver(err chan<- error, server *http.Server) {
	e := server.ListenAndServe()
	err <- e
}

var me uplink.DeviceMetadata
var d device.Device
var errorChannel = make(chan error)

func statusHandler(w http.ResponseWriter, r *http.Request) {
	b, err := json.Marshal(d.Status())
	if err != nil {
		errorText := fmt.Sprintf("could not marshal status, %v", err)
		http.Error(w, errorText, http.StatusBadRequest)
		logrus.Error(errorText)
		errorChannel <- err
		return
	}
	w.Write(b)
}

func configureHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errorText := fmt.Sprintf("could not readall body, %v", err)
		http.Error(w, errorText, http.StatusBadRequest)
		logrus.Error(errorText)
		errorChannel <- err
		return
	}
	defer r.Body.Close()
	p := struct{ Parameter string }{}
	err = json.Unmarshal(body, &p)
	if err != nil {
		errorText := fmt.Sprintf("could not unmarshal body, %v", err)
		http.Error(w, errorText, http.StatusBadRequest)
		logrus.Error(errorText)
		errorChannel <- err
		return
	}
	switch p.Parameter {
	case "datarate":
		value := struct{ Value byte }{}
		err = json.Unmarshal(body, &value)
		if err != nil {
			errorText := fmt.Sprintf("could not unmarshal body, %v", err)
			http.Error(w, errorText, http.StatusBadRequest)
			logrus.Error(errorText)
			errorChannel <- err
			return
		}
		d.SetDatarate(value.Value)
	case "adrackreq":
		if err != nil {
			errorText := fmt.Sprintf("could not unmarshal body, %v", err)
			http.Error(w, errorText, http.StatusBadRequest)
			logrus.Error(errorText)
			errorChannel <- err
			return
		}
		d.SetAdrackreq()
	case "linkcheckreq":
		if err != nil {
			errorText := fmt.Sprintf("could not unmarshal body, %v", err)
			http.Error(w, errorText, http.StatusBadRequest)
			logrus.Error(errorText)
			errorChannel <- err
			return
		}
		d.SetLinkCheckReq()
	case "fcntdown":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			errorText := fmt.Sprintf("could not unmarshal body, %v", err)
			http.Error(w, errorText, http.StatusBadRequest)
			logrus.Error(errorText)
			errorChannel <- err
			return
		}
		defer r.Body.Close()
		value := struct{ Value uint32 }{}
		err = json.Unmarshal(body, &value)
		if err != nil {
			errorText := fmt.Sprintf("could not unmarshal body, %v", err)
			http.Error(w, errorText, http.StatusBadRequest)
			logrus.Error(errorText)
			errorChannel <- err
			return
		}
		d.SetDnCtr(value.Value)
	}
}

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "starts simulated device",
	Long:  "starts a simulated device which implements the LoRaWAN stack",
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := conf.LoadConfig(v)
		if err != nil {
			return fmt.Errorf("load config failed: %w", err)
		}
		if len(config.LoriotServerURL) == 0 {
			return fmt.Errorf("loriot server url is not set")
		}
		if len(config.LoriotServerAPIKey) == 0 {
			return fmt.Errorf("loriot server api key is not set")
		}
		switch config.Registermethod {
		case "auto", "self":
			switch config.Joinmethod {
			case "ABP":
				err = loriot.RegisterABPDevice(config.LoriotServerURL,
					config.LoriotServerAPIKey, config.Deveui, config.Devaddr,
					config.Nwkskey, config.Appskey, config.Title,
					config.Counterup, config.Counterdown)
				if err != nil {
					return fmt.Errorf("register as abp failed: %w", err)
				}
			case "OTAA":
				err = loriot.RegisterOTAADevice(config.LoriotServerURL,
					config.LoriotServerAPIKey, config.Deveui, config.Appeui,
					config.Appkey, config.Title)
				if err != nil {
					return fmt.Errorf("register as otaa failed: %w", err)
				}
			default:
				return fmt.Errorf("unknown join method")
			}
			fields := logrus.Fields{
				"deveui":          config.Deveui,
				"loriotserverurl": config.LoriotServerURL,
				"joinmethod":      config.Joinmethod,
			}
			logrus.WithFields(fields).Infoln("device is registered")

		case "none", "other":
			//do nothing
		default:
			return fmt.Errorf("unknown register method")
		}
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		handler := mux.NewRouter()
		handler.HandleFunc("/status", statusHandler)
		handler.HandleFunc("/configure", configureHandler)
		server := &http.Server{Addr: fmt.Sprintf("%s:%d", config.DeviceserverURL, config.Deviceserverport), Handler: handler}

		go starthttpserver(errorChannel, server)
		defer server.Shutdown(context.Background())

		eui := config.Deveui
		if len(eui) != 16 {
			return fmt.Errorf("deveui is not valid: %s", eui)
		}
		b := broker.New(config.RabbitMQUrl, config.RabbitMQPort,
			config.RabbitMQUsername, config.RabbitMQPassword, config.Deviceupqueuename, eui)
		defer b.Close()

		err = b.Connect()
		if err != nil {
			return fmt.Errorf("connection to broker failed: %w", err)
		}

		me = uplink.DeviceMetadata{
			Deveui:    eui,
			Longitude: float32(config.Longitude),
			Latitude:  float32(config.Latitude),
		}
		me.Groups = make([]uint32, len(config.Groups))
		for i := 0; i < len(config.Groups); i++ {
			me.Groups[i] = uint32(config.Groups[i])
		}
		err = environmentadd(config)
		if err != nil {
			return fmt.Errorf("adding myself to environment failed: %w", err)
		}

		defer environmentremove(config)

		switch id := config.Program; id {
		case 0:
			deveui, err := util.String2Bytes8(eui)
			if err != nil {
				return fmt.Errorf("deveui parsing failed: %w", err)
			}
			switch config.Joinmethod {
			case "ABP":
				devaddr, err := util.String2Bytes4(config.Devaddr)
				if err != nil {
					return fmt.Errorf("devaddr parsing failed: %w", err)
				}
				nwkskey, err := util.String2Bytes16(config.Nwkskey)
				if err != nil {
					return fmt.Errorf("nwkskey parsing failed: %w", err)
				}
				appskey, err := util.String2Bytes16(config.Appskey)
				if err != nil {
					return fmt.Errorf("appskey parsing failed: %w", err)
				}

				d, err = device.CreateABP(b, deveui, devaddr, nwkskey, appskey, region.Parse(config.Region),
					lversion.ParseLoRaWAN(config.VersionLoRaWAN), lversion.ParseRegion(config.VersionRegion))
				if err != nil {
					return fmt.Errorf("creating abp device failed: %w", err)
				}
				defer d.Close()
				logrus.Info("device successfully initialized")
				err = d.JoinABP()
				if err != nil {
					return fmt.Errorf("device initial abp join failed: %w", err)
				}
			case "OTAA":
				appeui, err := util.String2Bytes8(config.Appeui)
				if err != nil {
					return fmt.Errorf("appeui parsing failed: %w", err)
				}
				appkey, err := util.String2Bytes16(config.Appkey)
				if err != nil {
					return fmt.Errorf("appkey parsing failed: %w", err)
				}
				d, err = device.CreateOTAA(b, deveui, appeui, appkey, region.Parse(config.Region),
					lversion.ParseLoRaWAN(config.VersionLoRaWAN), lversion.ParseRegion(config.VersionRegion))
				if err != nil {
					return fmt.Errorf("creating otaa device failed: %w", err)
				}
				defer d.Close()
				logrus.Info("device successfully initialized")
				err = d.JoinOTAA()
				if err != nil {
					return fmt.Errorf("device initial otaa join failed: %w", err)
				}
			default:
				return fmt.Errorf("unknown join method")
			}
			d.SetAdr(config.Adr)
			for i := uint(0); i < config.Rejoinretry; i++ {
				select {
				case <-time.After(time.Duration(config.Rejoininterval) * time.Second):
				case <-c:
					return nil
				}

				if !d.Status().Joined {
					switch config.Joinmethod {
					case "ABP":
						err = d.JoinABP()
						if err != nil {
							return fmt.Errorf("device abp join failed: %w", err)
						}
					case "OTAA":
						err = d.JoinOTAA()
						if err != nil {
							return fmt.Errorf("device otaa join failed: %w", err)
						}
					default:
						return fmt.Errorf("unknown join method")
					}
				} else {
					break
				}
			}
			if !d.Status().Joined {
				logrus.Debug("device failed to join, gives up existence")
				return nil
			}
			uplinkpayloadbytes, err := hex.DecodeString(config.UplinkPayload)
			if err != nil {
				return fmt.Errorf("cannot decode uplink payload hex string: %w", err)
			}
			fport := byte(config.Uplinkport)
			confirmed := config.UplinkConfirmed
			uplinkinterval := config.Uplinkinterval
			for {
				err := d.Uplink(confirmed, &fport, uplinkpayloadbytes)
				if err != nil {
					if err != types.IsBusy {
						return fmt.Errorf("device is busy: %w", err)
					}
					logrus.Debug("device is busy")
				}
				select {
				case <-c:
					return nil
				case <-time.After(time.Duration(uplinkinterval) * time.Second):
				case e := <-errorChannel:
					return e
				}
			}
		default:
			return fmt.Errorf("program with id %d does not exist", id)
		}
	},
}
