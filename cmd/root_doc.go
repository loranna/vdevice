package cmd

import (
	"gitlab.com/loranna/configuration/docs"
)

func GenerateDocumentation() error {
	err := docs.Generate(rootCmd)
	if err != nil {
		return err
	}
	return nil
}
