package util

import "encoding/hex"

func String2Bytes8(input string) ([8]byte, error) {
	slice, err := hex.DecodeString(input)
	if err != nil {
		return [8]byte{}, err
	}
	var array [8]byte
	copy(array[:], slice)
	return array, nil
}

func String2Bytes4(input string) ([4]byte, error) {
	slice, err := hex.DecodeString(input)
	if err != nil {
		return [4]byte{}, err
	}
	var array [4]byte
	copy(array[:], slice)
	return array, nil
}

func String2Bytes16(input string) ([16]byte, error) {
	slice, err := hex.DecodeString(input)
	if err != nil {
		return [16]byte{}, err
	}
	var array [16]byte
	copy(array[:], slice)
	return array, nil
}
