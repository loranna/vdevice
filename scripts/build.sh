#!/bin/bash
VERSION=$MAJOR.$MINOR.$COMMITCOUNT
if [ -z ${COMMITCOUNT+x} ]; then
    #this happens when no env variable is set
    VERSION=$(git rev-parse --short HEAD)
fi

go build -ldflags "-X 'gitlab.com/loranna/vdevice/cmd.version=$VERSION'" \
-o vdevice
