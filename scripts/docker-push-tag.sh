#!/bin/bash

TAG=$(git tag -l --sort=-v:refname | head -n 1 | cut -c 2-)
IFS='.' read -ra vers <<< "$TAG"
MAJOR="${vers[0]}"
MINOR="${vers[1]}"
COMMITCOUNT="${vers[2]}"

docker push registry.gitlab.com/loranna/vdevice:$MAJOR.$MINOR.$COMMITCOUNT

#VERSION=$MAJOR.$MINOR.$COMMITCOUNT
#curl -X POST https://slack.com/api/chat.postMessage \
#-H "Content-type: application/json;charset=utf-8" \
#-H "Authorization: Bearer $SLACK_TOKEN" \
#-d  "{\"channel\":\"$SLACK_CHANNEL\",\"text\":\"new version of device is released: $VERSION\"}"