package broker

import (
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/gogo/protobuf/proto"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	device_radio "gitlab.com/loranna/vdevice/radio"
	"gitlab.com/loranna/vdevice/radioconv"
	"gitlab.com/loranna/venvironment/uplink"
)

type Broker struct {
	username, password, url string
	port                    uint
	conn                    *amqp.Connection

	uplinkqueuename, deveui string
	channelup, channeldown  *amqp.Channel
}

func New(url string, port uint, username, password string,
	uplinkqueuename, deveui string) *Broker {
	broker := Broker{}
	broker.username = username
	broker.password = password
	broker.url = url
	broker.port = port
	broker.uplinkqueuename = uplinkqueuename
	broker.deveui = deveui
	return &broker
}

func (b *Broker) Connect() error {
	server := fmt.Sprintf("amqp://%s:%s@%s:%d/",
		b.username, b.password, b.url, b.port)
	c, err := amqp.Dial(server)
	if err != nil {
		return err
	}
	b.conn = c
	ch, err := b.conn.Channel()
	if err != nil {
		return err
	}
	b.channelup = ch
	ch, err = b.conn.Channel()
	if err != nil {
		return err
	}
	b.channeldown = ch
	_, err = ch.QueueDeclare(
		"dev"+b.deveui, // name
		false,          // durable
		true,           // delete when unused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)
	if err != nil {
		return fmt.Errorf("queue declare failed: %v", err)
	}
	return nil
}

func (b *Broker) Close() error {
	// consume ranges are closed by the conn.Close
	// see end of example: https://godoc.org/github.com/streadway/amqp#example-Channel-Consume
	if b.conn != nil {
		return b.conn.Close()
	}
	return nil
}

func (b *Broker) Send(radio device_radio.Parameters, payload []byte, deveui string) error {
	up := uplink.WithMetadataFromDevice{
		Payload:                hex.EncodeToString(payload),
		Frequency:              radio.Frequency,
		Modulation:             radioconv.RadioModtouplink(radio.Modulation),
		Bandwidth:              radioconv.Inttobandwidth(radio.Bandwidth),
		Spreadingfactor:        radioconv.InttoSF(radio.SpreadingFactor),
		Coderate:               radioconv.RadioCRtoUplink(radio.CodeRate),
		Payloadsize:            uint32(len(payload)),
		Transmitsignalstrength: float64(radio.TxSignalStrength),
		Deveui:                 deveui,
	}

	upbytes, err := proto.Marshal(&up)
	if err != nil {
		return err
	}
	err = b.channelup.Publish(
		"",                // exchange
		b.uplinkqueuename, // routing key
		false,             // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         upbytes,
		})
	if err != nil {
		return err
	}
	logrus.WithField("epayload", strings.ToUpper(up.GetPayload())).Trace("Sent payload")
	return nil
}

func (b *Broker) Receive() (<-chan uplink.WithMetadataToDevice, error) {
	rgwdn, err := b.channeldown.Consume(
		"dev"+b.deveui, // queue
		"",             // consumer
		false,          // auto-ack
		false,          // exclusive
		false,          // no-local
		false,          // no-wait
		nil,            // args
	)
	if err != nil {
		return nil, err
	}
	chann := make(chan uplink.WithMetadataToDevice)
	go func() {
		for d := range rgwdn {
			u := uplink.WithMetadataToDevice{}
			proto.Unmarshal(d.Body, &u)
			chann <- u
			d.Ack(false)
		}
		close(chann)
	}()
	return chann, nil
}
